# Soccer Ranking Project

## Building the project

1. [Ensure you have Stack installed](https://docs.haskellstack.org/en/stable/install_and_upgrade/)
1. In your shell, `cd` to the project root and run `stack build`
1. If this is your first time building, it could take a while as stack may have
   to download and build some of the dependencies.

## Running the project

1. In your shell, `cd` to the project root and run either 
   `stack exec soccer-ranking-project-exe -- io-files/input0.txt`
   or `stack exec soccer-ranking-project-exe -- io-files/input-sample.txt`
1. The results should be printed out to `out.txt` in the project root.

## Running the test suite

1. In your shell, `cd` to the project root and run `stack test`.

## How much time it took me

The core implementation took me about 2-2.5 hours.  I had some trouble in the
beginning remembering how to get the OverloadedStrings GHC extension to behave,
after that, things went more smoothly.  Setting-up the environment and the tests
took me a bit of time, maybe an hour, to come up with some decent test cases.
If I were more comfortable with QuickCheck, it would have gone faster.  I've used
ScalaCheck and Hypothesis in the past with success, but after staring at
QuickCheck for a while tonight I abandoned it and just wrote some basic HSpec
tests instead.  We can discuss this more later if you would like.

## Review for Improvements

1. It would be nice to create a nicer CLI args experience using flags and to
   provide an option for where to put the output file.  For such a simple
   program that will never get used again, I thought the CLI interface was one
   of the least important things. If one wanted to go overboard, though, the
   Applicative DSL can be used to create a pretty nifty way of parsing through
   the CLI args in a way that provides defaults and optional arguments.
1. There's probably some performance improvements that could be made if we wanted
   to get crazy. Some obvious points that aren't likely ideal is executing an IO
   action for each line of output as a file append. I didn't change this as it
   did not seem that important for this program (seemed a bit nit-picky), but I
   kind-of wanted to go back and just concat the final
   list of `T.Text`s into a single `T.Text` and output the entire file contents
   in a single action since the output is so small. I'm sure something like this
   would be caught in code review.
1. Some of the `List`s in the
   program are also traversed more than once. I thought about changing that,
   but I decided against it in the end because I thought it would make the code
   less readable and performance isn't really an issue with this code at the
   moment. It would also be nice to do some profiling to most likely prove that
   using a Strict Map and Data.Vector and Data.Text were all overkill.  I went
   ahead and used those libraries/types anyway just to demonstrate that I'm
   aware of them.
1. I'd like to get some property testing in place. QuickCheck was a bit much for
   me to pick up again today, apparently, but I've also heard that some people
   in the community are moving away from QuickCheck and to some newer libraries
   like [Hedgehog](https://hackage.haskell.org/package/hedgehog). It would be
   interesting to explore how that library could assist with some of the test
   quality.
1. There's currently no automated "integration" tests that actually test the
   IO portions of the code or the entire application from start to finish. It
   would be nice to add that in.
1. As always, some of the functions in the code could probably be named better.
1. Also, again, for such a simple program, I thought that going full MTL and
   creating a fully specialized type system for this application would be
   overkill as well. But I also know ideally one would stear away from doing
   everything effectual in the IO monad. If this were going to become a larger
   application, it might be a good idea to use Readers to manage the info about
   the environments impacting the runtime of the program, including, but not
   limited to, the CLI args. Taken to the extreme, the program could be written
   such that `main = runReader RankSoccerTeams` runs the entire program or
   something (in Scala this normally looks like `RankSoccerTeams.run`).
1. I hope I get to look forward to a discussion about this implementation.
