{-# LANGUAGE OverloadedStrings #-}

module RankSoccerTeams
    ( rankSoccerTeams
    , calcRawScores
    , reduceRawScores
    , sortFinalScores
    , formatOutputLines
    , performRanking
    ) where

import System.Environment
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Data.Map.Strict as M
import qualified Data.Vector as V
import Data.Sort (sortBy)

rankSoccerTeams :: IO [()]
rankSoccerTeams =
  getArgs
  >>= \args -> (fmap T.lines . TIO.readFile) (head args)
  >>= \fileContent ->
        sequence $ map (\s -> TIO.appendFile "./out.txt" (s <> "\n"))
                 $ performRanking fileContent

toTuple2 :: [a] -> (a, a)
toTuple2 [first, second] = (first, second)

-- calcRawScores takes the raw input lines and turns them into a list of score
-- events for each team.  If there's a clear winner on the current line, then the
-- winning team will get a row with the score of 3 associated with it, and the
-- loser a row with 0.  If it was a tie, then each team gets a row with a score
-- of 1.
calcRawScores :: [T.Text] -> [(T.Text, Int)]
calcRawScores =
  let splitScores = map T.strip . T.split (== ',') -- divides the line between the 2 teams
      splitTeamText = V.fromList . T.split (== ' ') -- ["team", "name", ..., "score"]
      parseLine = toTuple2 . map splitTeamText . splitScores
      getTeamName whichTeam = T.intercalate " " . V.toList . V.init . whichTeam
      calcScore parsedLine =
        let score1 = read $ T.unpack $ V.last $ fst parsedLine :: Int
            score2 = read $ T.unpack $ V.last $ snd parsedLine :: Int
            firstTeam = getTeamName fst parsedLine
            secondTeam = getTeamName snd parsedLine
        in case compare score1 score2 of
            LT -> [ (secondTeam, 3)
                  , (firstTeam, 0)
                  ]
            GT -> [ (firstTeam, 3)
                  , (secondTeam, 0)
                  ]
            EQ -> [ (firstTeam, 1)
                  , (secondTeam, 1)
                  ]
  in concat . map (calcScore . parseLine)

reduceRawScores :: [(T.Text, Int)] -> [(T.Text, Int)]
reduceRawScores = M.toList . M.fromListWith (+)

sortFinalScores :: [(T.Text, Int)] -> [(T.Text, Int)]
sortFinalScores = sortBy $ \(teamName1, score1) (teamName2, score2) ->
    let compareResult = compare score2 score1 -- order flipped for desc ordering
    in case compareResult of
      EQ -> compare teamName1 teamName2 -- order "normal" for ascending ordering
      _ -> compareResult

formatOutputLines :: [(T.Text, Int)] -> [T.Text]
formatOutputLines scores =
  map (\(teamName, score) ->
         let initLine = teamName <> ", " <> (T.pack $ show score)
         in if score == 1 then initLine <> " pt" else initLine <> " pts"
      )
      scores

performRanking :: [T.Text] -> [T.Text]
performRanking =
  formatOutputLines . sortFinalScores . reduceRawScores . calcRawScores
