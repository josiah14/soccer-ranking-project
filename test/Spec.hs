{-# LANGUAGE OverloadedStrings #-}

import Test.Hspec
import qualified Data.Set as S
import RankSoccerTeams

main :: IO ()
main = hspec $ do
  describe "RankSoccerTeams.reduceRawScores" $ do
    it "Reduces the tuples list by key using the sum (+) operator" $
      let rawScores = [ ("Tarantulas", 3)
                      , ("FC Awesome", 1)
                      , ("Lions", 1)
                      , ("FC Awesome", 3)
                      , ("Snakes", 1)
                      , ("Tarantulas", 1)
                      , ("Memphis 901 FC", 0)
                      , ("Memphis 901 FC", 0)
                      , ("Sporting KC", 3)
                      , ("FC Awesome", 1)
                      , ("Charleston Battery", 3)
                      , ("Sporting KC", 3)
                      , ("Memphis 901 FC", 0)
                      , ("FC Awesome", 1)
                      , ("Sporting KC", 3)
                      , ("Atlanta United 2", 1)
                      , ("Grouches", 1)
                      , ("Memphis 901 FC", 0)
                      , ("FC Awesome", 1)
                      , ("Sporting KC", 3)
                      , ("Sporting KC", 1)
                      , ("Miami FC", 1)
                      , ("Underdog", 0)
                      ]
          expectedOutput = [ ("Tarantulas", 4)
                           , ("FC Awesome", 7)
                           , ("Lions", 1)
                           , ("Snakes", 1)
                           , ("Memphis 901 FC", 0)
                           , ("Sporting KC", 13)
                           , ("Charleston Battery", 3)
                           , ("Atlanta United 2", 1)
                           , ("Miami FC", 1)
                           , ("Grouches", 1)
                           , ("Underdog", 0)
                           ]
      in (S.fromList $ reduceRawScores rawScores) `shouldBe` S.fromList expectedOutput

  describe "RankSoccerTeams.reduceRawScores" $ do
    it "Translates each Text in the List into a pair of scores" $
      let rawTextLines = [ "Atlanta United 2 5, Miami FC 2"
                         , "Charleston Battery 0, Miami FC 2"
                         , "Memphis 901 FC 2, Miami FC 2"
                         , "Miami FC 0, Sporting Kansas City II 1"
                         , "Memphis 901 FC 3, Atlanta United 2 2"
                         , "Sporting Kansas City II 0, Memphis 901 FC 0"
                         , "Atlanta United 2 1, Charleston Battery 0"
                         , "Atlanta United 2 10, Sporting Kansas City II 6"
                         , "Charleston Battery 7, Sporting Kansas City II 11"
                         , "Memphis 901 FC 10, Charleston Battery 10"
                         , "Lions 3, Snakes 3"
                         , "Tarantulas 1, FC Awesome 0"
                         , "Lions 1, FC Awesome 1"
                         , "Tarantulas 3, Snakes 1"
                         , "Lions 4, Grouches 0"
                         ]
          expectedOutput = [ ("Atlanta United 2", 3), ("Miami FC", 0)
                           , ("Charleston Battery", 0), ("Miami FC", 3)
                           , ("Memphis 901 FC", 1), ("Miami FC", 1)
                           , ("Miami FC", 0), ("Sporting Kansas City II", 1)
                           , ("Memphis 901 FC", 3), ("Atlanta United 2", 0)
                           , ("Sporting Kansas City II", 1), ("Memphis 901 FC", 1)
                           , ("Atlanta United 2", 3), ("Charleston Battery", 0)
                           , ("Atlanta United 2", 3), ("Sporting Kansas City II", 0)
                           , ("Charleston Battery", 0), ("Sporting Kansas City II", 3)
                           , ("Memphis 901 FC", 1), ("Charleston Battery", 1)
                           , ("Lions", 1), ("Snakes", 1)
                           , ("Tarantulas", 3), ("FC Awesome", 0)
                           , ("Lions", 1), ("FC Awesome", 1)
                           , ("Tarantulas", 3), ("Snakes", 0)
                           , ("Lions", 3), ("Grouches", 0)
                           ]
      in (S.fromList $ calcRawScores rawTextLines) `shouldBe` S.fromList expectedOutput

  describe "RankSoccerTeams.formatOutputLines" $ do
    it "Correctly puts 'pt' after scores of 1 and 'pts' after other scores" $
      let finalScores = [ ("Tarantulas", 4)
                        , ("FC Awesome", 7)
                        , ("Lions", 1)
                        , ("Snakes", 1)
                        , ("Memphis 901 FC", 0)
                        , ("Sporting KC", 13)
                        , ("Charleston Battery", 3)
                        , ("Atlanta United 2", 1)
                        , ("Miami FC", 1)
                        , ("Grouches", 1)
                        , ("Underdog", 0)
                        ]
          expectedOutput = [ ("Tarantulas, 4 pts")
                           , ("FC Awesome, 7 pts")
                           , ("Lions, 1 pt")
                           , ("Snakes, 1 pt")
                           , ("Memphis 901 FC, 0 pts")
                           , ("Sporting KC, 13 pts")
                           , ("Charleston Battery, 3 pts")
                           , ("Atlanta United 2, 1 pt")
                           , ("Miami FC, 1 pt")
                           , ("Grouches, 1 pt")
                           , ("Underdog, 0 pts")
                           ]
      in (S.fromList $ formatOutputLines finalScores) `shouldBe` S.fromList expectedOutput

  describe "RankSoccerTeams.sortFinalScores" $ do
    it "Correctly sorts the final scores first by score descending then alphabetically" $
      let finalScores = [ ("Tarantulas", 4)
                        , ("FC Awesome", 7)
                        , ("Lions", 1)
                        , ("Snakes", 1)
                        , ("Memphis 901 FC", 0)
                        , ("Sporting KC", 13)
                        , ("Charleston Battery", 3)
                        , ("Atlanta United 2", 1)
                        , ("Miami FC", 1)
                        , ("Grouches", 1)
                        , ("Underdog", 0)
                        ]
          expectedOutput = [ ("Sporting KC", 13)
                           , ("FC Awesome", 7)
                           , ("Tarantulas", 4)
                           , ("Charleston Battery", 3)
                           , ("Atlanta United 2", 1)
                           , ("Grouches", 1)
                           , ("Lions", 1)
                           , ("Miami FC", 1)
                           , ("Snakes", 1)
                           , ("Memphis 901 FC", 0)
                           , ("Underdog", 0)
                           ]
      in sortFinalScores finalScores `shouldBe` expectedOutput
